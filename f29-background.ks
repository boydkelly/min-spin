#let's start customizing the desktop

%packages

f29-backgrounds-gnome  #This only installs the f29 default background.  But it doesn't set it.... Weird!  Lets do that here.

%end

%post

cat >> /usr/share/glib-2.0/schemas/org.gnome.desktop.background.gschema.override << FOE
[org.gnome.desktop.background]
picture-uri='file:////usr/share/backgrounds/f29/default/f29.xml'
color-shading-type='solid'
primary-color='#425265'
picture-options='wallpaper'
secondary-color='#425265'
FOE

%end
