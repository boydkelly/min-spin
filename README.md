# min-spin

Fedora 29 absolute minimal gnome desktop.  The only thing you get here is gnome-terminal and nautilus file manager.  That's it.  Nothing else.  The kickstart files are divided into 'minimization' and 'packages'.  If you need hardware support you can enable firmware and xorg drivers for your hardware.  Currently this will give you a lightweight gnome 3 desktop with only 653 packages.  Add an application of your choice and this ismakes a great usb key.
