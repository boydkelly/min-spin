%packages
# Exclude unwanted groups that fedora-live-base.ks pulls in
# Exactly!  Exclude them all!!!!
@base-x
# Not base enough!  See minimization.ks
@core
#Install only the fonts you need/want.  I need terminus for my hidpi screen
# Oddly enough with a super min install, gnome desktop3 won't require its default fonts.... Go figure... with all the other dependencies...
liberation-mono-fonts
liberation-sans-fonts
liberation-serif-fonts
terminus-fonts-console

#choose your xorg drivers below
#xorg-x11-apps
#xorg-x11-drv-ati
#xorg-x11-drv-fbdev
#xorg-x11-drv-intel
#xorg-x11-drv-vmware
#xorg-x11-drv-nouveau
#xorg-x11-drv-vesa
#xorg-x11-drv-wacom
#xorg-x11-drv-openchrome
#xorg-x11-drv-wacom-serial-support
#xorg-x11-drv-evdev
#xorg-x11-drv-qxl

#specific firmware only
#linux-firmware
#iwl7260-firmware

#This will run fine under kvm with virtio drivers.  If you want qxl or another platform uncomment below.
#hyperv-daemons
#open-vm-tools-desktop
#virtualbox-guest-additions
#qemu-guest-agent
#spice-vdagent
#xorg-x11-drv-qxl

#The meat
#add your language below... ie glibc-langpack-fr etc.  Gnome Terminal won't start with only the glibc-minimal-langpack.  Go figure...
glibc-langpack-en
#glibc-minimal-langpack
gnome-shell
gnome-desktop3
gdm
authselect-compat
gnome-session-wayland-session
xdg-user-dirs   #needed to create folders in home directory
NetworkManager-wifi

#The potatoes  
gnome-terminal
nautilus
#just because...
#-vim-minimal
#neovim
#wget
#mailx
#ssmtp
#chromium
#epiphany
#dconf-editor
#gnome-tweaks

#add suuport for efi boot
#shim-x64
grub2-efi-x64
grub2-efi-x64-cdboot
grub2-tools-efi
efibootmgr
efivar
efi-filesystem
mactel-boot
#isomd5sum

%end

