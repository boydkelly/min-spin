%packages
-@guest-desktop-agents
-@standard
-@fonts  
-@input-methods
-@dial-up
-@multimedia
-@hardware-support
-@printing

-@anaconda-tools
-anaconda
-anaconda-install-env-deps

-mpage
-sox
-hplip
-numactl
-isdn4k-utils
-autofs
-gfs2-utils
-reiserfs-utils
-aajohan-comfortaa-fonts
-xsane  #who scans anymore anyway!!
-xsane-gimp
-sane-backends
-glibc-all-langpacks
-man-pages
-man-pages-*
-tiger-vncserver-minimal
-deltarpm
-dnf-plugins-core
-dnf-yum
-vino
-grubby
#-hunspell
#-hunspell-*
#-enchant
#-abattis-cantarell-fonts
#-google-noto-emoji-color-fonts  #really????

#slim down xorg.....  If you need a specific driver remove the - below. (What is commented here is required)
-abrt-addon-xorg
#xorg-x11-xkb-utils
#xorg-x11-server-utils
#xorg-x11-font-utils
#xorg-x11-xinit
#xorg-x11-xauth
#xorg-x11-drv-libinput
#xorg-x11-server-Xwayland
#xorg-x11-server-common
#xorg-x11-server-Xorg
-xorg-x11-drv-ati
-xorg-x11-drv-fbdev
-xorg-x11-drv-intel
-xorg-x11-drv-vmware
-xorg-x11-drv-nouveau
-xorg-x11-drv-vesa
-xorg-x11-drv-wacom
-xorg-x11-drv-openchrome
-xorg-x11-drv-wacom-serial-support
-xorg-x11-drv-evdev
-xorg-x11-drv-qxl

-xorg-x11-apps
-xorg-x11-fonts-ISO8859-1-100dpi
-xorg-x11-fonts-misc
-xorg-x11-fonts-Type1

-xorg-x11-proto-devel
-xorg-x11-server-Xephyr
-xorg-x11-util-macros
-xorg-x11-utils
-xorg-x11-xbitmaps
-xorg-x11-xkb-extras

-mesa-vulkan-drivers
mesa-libGLES
mesa-libGLU
mesa-libOSMesa
#-mesa-libxatracker

#Slim down NetworkManager
-NetworkManager-team

#Virt host and guest stuff
-gnome-boxes
-libvirt-*
-hyperv-daemons
-open-vm-tools-desktop
-virtualbox-guest-additions
qemu-guest-agent
spice-vdagent

%end
